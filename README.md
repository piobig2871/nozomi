# Nozomi Networks interview

## Python version
Python 3.7.9

## Get the repository 

```buildoutcfg
git clone git@gitlab.com:piobig2871/nozomi.git
cd nozomi
```

## Install requierements
```buildoutcfg
python3 -m pip install -r requierements.txt
```
